<?php
$title = "About";
include "includes/header.php";
include "includes/navbar.php";
?>

<h1> About </h1> 
<p> pb is a pastebin service that supports markdown and plain text. The server side software is licensed under the <a href="/license.php">BSD 2-Clause</a> license. </p>

<h1> Usage </h1>
<p> The markdown processor is in-house. It doesn't support the full markdown spec but it supports pre tags, single level lists, images, links, inline emphasizers, hr tags, and tables. To prevent the use of iframes and javascript, all markdown input is stripped of tags. </p> 

<?
include "includes/footer.php";
?>
