<?php
$title = "submit";
session_start();
include "includes/header.php";
include "includes/navbar.php";
include "includes/insert.php";

if($_POST['captcha'] == $_SESSION['capcode']){
	echo "<h1>Success!</h1> ";
	$id = insert($_SERVER["REMOTE_ADDR"], $_POST['title'], $_POST["md"]);
	session_unset();
	session_destroy();
	header("Refresh: 3; url=http://" . $_SERVER["HTTP_HOST"] . "/index.php?q=" . $id);
} else {
	echo "<h1>Captcha Incorrect</h1>";
	session_unset();
	session_destroy();
	header("Refresh: 3; url=http://" . $_SERVER["HTTP_HOST"]);
}
include "includes/footer.php";
?>
