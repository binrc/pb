<?php
$title = "License";
include "includes/md2htm.php";
include "includes/header.php";
include "includes/navbar.php";
$file = "assets/docs/LICENSE.md";
$fp = fopen($file, "r") or die;
echo md2htm(fread($fp,filesize($file)));
fclose($fp);
include "includes/footer.php";
?>
