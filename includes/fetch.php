<?php

function fetch($id){
	include "dbcon.php";

	$db = new mysqli($sqlhost, $user, $PB_DB_PASS, $database);
	$st = $db->prepare("select * from pb.mds where id = ? limit 1");

	if(!$st){
		echo "prepare statement failure";
	} else {
		$st->bind_param("s", $id);
		$st->execute();
		$res = $st->get_result();
	}

	$db->close();

	return $res;
}
?>
