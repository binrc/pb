<?php
function drawLines($img, $x, $y){
	$xmin = rand(0, $x);
	$xmax = rand($x/2, $x);
	$ymin = rand(0, $y);
	$ymax = rand($y/2, $y);

	// random hex code
	//$color = bin2hex(random_bytes(3));
	$color = "000000";

	$draw = new ImagickDraw();
	$draw->setFillColor("#" . $color);
	//$draw->line($xmin, $ymin, $xmax, $ymax);
	$draw->line(rand(0, $x/2), rand(0, $y/2), rand($x*1.5, $y), rand($y*1.5, $y));
	$img->drawImage($draw);
	
}

function drawPoints($img, $x, $y){
	// random hex code
	//$color = bin2hex(random_bytes(3));
	$color = "000000";

	$draw = new ImagickDraw();
	$draw->setFillColor("#" . $color);
	$draw->point(rand(0, $x), rand(0, $y));
	$img->drawImage($draw);
	
}



function gcapcode(){
	//header('Content-type: image/png');
	$image = new Imagick();
	$draw = new ImagickDraw();
	$x = 140;
	$y = 40;
	$image->newImage($x, $y, new ImagickPixel("white"));
	// plasma one
	//$image->newPseudoImage($x, $y, "plasma:fractal");
	//$image->addNoiseImage(7, 14);
	
	// add static
	/*
	for($i=0; $i<999; $i++){
		drawPoints($image, $x, $y);
	}
	 */
	
	$image->addNoiseImage(imagick::NOISE_MULTIPLICATIVEGAUSSIAN, imagick::CHANNEL_DEFAULT);
	
	// add capcode
	$capcode = bin2hex(random_bytes(4));
	$draw->setFillColor("#131313");
	$draw->setFont("assets/fonts/font");
	$draw->setFontSize(26);
	$image->annotateImage($draw, 5, 30, 0, $capcode);
	
	// add lines
	for($i=0; $i<10; $i++){
		drawLines($image, $x, $y);
	}
	
	// finalize
	$image->swirlImage(20);
	$image->implodeImage(0.2);
	$image->setImageFormat('png');
	echo "<img class='capimg' src='data:image/png;charset=utf8;base64, " . base64_encode($image->getImageBlob()) . "'/>\n";
	return $capcode;
}
?>
