<?php

function insert($ip, $title, $md){
	include "dbcon.php";

	$db = new mysqli($sqlhost, $user, $PB_DB_PASS, $database);
	$st = $db->prepare("insert into pb.mds(ip, title, md) values(?, ?, ?)");

	if(!$st){
		echo "prepare statement failure";
	} else {
		$st->bind_param("sss", $ip, htmlentities(strip_tags($title)), htmlentities(strip_tags($md)));
		$st->execute();
		$id=$st->insert_id;
	}

	$db->close();

	return $id;
}
?>
