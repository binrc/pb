drop database if exists pb;
drop user if exists "pb"@"127.0.0.1";
create user if not exists "pb"@"127.0.0.1" identified by "worstpractices";
create database if not exists pb;
grant all privileges on pb.* to "pb"@"127.0.0.1";
flush privileges;

create table if not exists pb.mds(
	id int auto_increment primary key not null,
	time timestamp not null default current_timestamp,
	ip varchar(45) not null,
	title tinytext not null,
	md text not null
)

