<?php
$title = "Remix";
include "includes/header.php";
include "includes/navbar.php";
if(empty($_GET)){
	echo "<h1> Nothing to remix </h1>";
	header("Refresh: 3; url=http://" . $_SERVER["HTTP_HOST"]);
	include "includes/footer.php";
	die;
} else {

session_start();
	include "includes/md2htm.php";
	include "includes/dbcon.php";
	include "includes/fetch.php";
	$res = fetch($_GET["q"]);
	$row = $res->fetch_array(MYSQLI_NUM);
	
	$id	= $row[0];
	$ts	= $row[1];
	$title	= $row[3];
	$md	= $row[4];
	
	echo '<form method="POST" action="submit.php" id="newmd">';
	echo '<label class="form" for="title"><h3>Title: </h3></label><br>';
	printf('<input class="form" name="title" type="text" required autocomplete="off" value="%s (remixed)"><br>', $title);
	echo '<label class="form" for="md"><h3>Markdown: </h3></label><br>';
	printf('<textarea spellcheck="true" rows="24" cols="80" name="md" form="newmd" required>%s</textarea><br>', $md);
	echo '<label class="form" for="captcha"><h3>Captcha:</h3></label><br>';
	
	include "includes/gcapcode.php";
	$_SESSION['capcode'] = gcapcode();
	
	echo '<input class="form" name="captcha" type="text" required pattern="[0-9a-f]+" autocomplete="off" value=""><br>';
	echo '<input type="submit" value="Submit" for="newmd">';
	echo '</form>';
	
	include "includes/footer.php";
}
?>
