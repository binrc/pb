# Dependencies
requires \*AMP stack with `pecl-imagick` and `php-mysqli`. After these things are installed, you should initialize the database. 

# Database setup
`sql/initdb.sql` is a script that will set up the database. You *MUST* change the password. This sed command will do the trick. 

```shell
$ sed -i'' -e "s/worstpractices/$realpassword/g" sql/initdb.sql
```

Now, initialize the database. 

```shell
$ mysql < sql/initdb.sql
```

# php.ini setup
To further secure the database, should move your password to your php.ini. Enable the modules like so:

```ini
extension=myslqi
extension=imagick.so
```

# securing the system
disable access (or preferably remove) `sql/`
